#!/bin/sh

mkdir -p /opt/Qt
cd /opt/Qt

QT_SERVER=http://download.qt.io
ANDROID_DIR=online/qtsdkrepository/linux_x64/android
QT_VERSION_DIR=qt5_5142

QT_URL=$QT_SERVER/$ANDROID_DIR/$QT_VERSION_DIR

FILE=5.14.2-0-202003291250qtpurchasing-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5142.qtpurchasing.android/$FILE
p7zip -d $FILE

FILE=5.14.2-0-202003291250qtnetworkauth-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5142.qtnetworkauth.android/$FILE
p7zip -d $FILE

FILE=5.14.2-0-202003291250qtdatavis3d-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5142.qtdatavis3d.android/$FILE
p7zip -d $FILE

FILE=5.14.2-0-202003291250qtcharts-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5142.qtcharts.android/$FILE
p7zip -d $FILE

FILES="5.14.2-0-202003291250qtxmlpatterns-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtwebview-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtwebsockets-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtwebchannel-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qttranslations-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qttools-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtsvg-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtspeech-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtserialport-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtsensors-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtscxml-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtquickcontrols2-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtquickcontrols-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtmultimedia-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtlocation-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtimageformats-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtgraphicaleffects-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtgamepad-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtdeclarative-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtconnectivity-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtbase-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qtandroidextras-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.2-0-202003291250qt3d-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z"
for FILE in $FILES
do
    wget -q $QT_URL/qt.qt5.5142.android/$FILE
    p7zip -d $FILE
done

# RPath etc. is set to /home/qt/work/install/lib - so doing a symlink
mkdir -p /home/qt/work
ln -s /opt/Qt/5.14.2/android /home/qt/work/install

# switch Qt license to OSS
#/opt/Qt/5.14.1/android_armv7/mkspecs/qconfig.pri
#QT_EDITION = Enterprise -> OpenSource
PRI_FILE="/opt/Qt/5.14.2/android/mkspecs/qconfig.pri"
sed -i 's/Enterprise/OpenSource/g' "${PRI_FILE}"
sed -i 's/licheck.*//g' "${PRI_FILE}"
